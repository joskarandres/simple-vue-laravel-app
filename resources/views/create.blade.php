<div class="modal fade" id="create">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>New Task</h4>
                <button type="button" class="close" data-dismiss = "modal">
                    <span>
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <label for="keep">Crear Tarea</label>
                <input type="text" name="keep" id="keep" class="form-control" v-model="newKeep">
                <span v-for="error in errors" class="text-danger">
                    @{{error}}
                </span>
            </div>
            <div class="modal-footer">
                <input type="submit" value="Save" class="btn btn-primary" @click="createKeep()">
            </div>
        </div>
    </div>
</div>