
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vue from 'vue';
import toastr from './toastr';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#crud',
    created: function(){
        this.getKeeps();
    },
    data:{
        keeps:[],
        newKeep:'',
        errors:[],
        fillKeep:{id: '', keep:''},
        pagination:{
            'total':0,
            'current_page':0,
            'per_page':0,
            'last_page':0,
            'from':0,
            'to':0,
        }
    },
    computed: {
        isActived: function(){
            return this.pagination.current_page;
        },

        pagesNumber: function(){
            if(!this.pagination.to){
                return[]
            }

            let from = this.pagination.current_page - 2; //TODO offset

            if(from<1)
                from = 1;
            
            let to = from + (2*2); //TODO offset

            if(to>this.pagination.last_page)
                to = this.pagination.last_page;
            
            let pagesArray = []
            while(from <= to){
                pagesArray.push(from);
                from++;
            }

            return pagesArray;
        }
    },
    methods:{
        updateKeep:function(id){
            let url = `task/${id}`;
            axios.put(url, this.fillKeep)
            .then(response => {
                this.getKeeps();
                this.fillKeep = {id:'', keep:''};
                this.errors  = [];
                $("#edit").modal("hide");
                toastr.success("Actualizado correctamente");
            })
            .catch(error => {
                this.errors = error.response.data;
            })
        },
        editKeep:function({id, keep}){
            this.fillKeep.id=id;
            this.fillKeep.keep = keep;
            $("#edit").modal("show");
            return;
        },
        getKeeps:function(page){
            console.log(page);
            let urlKeeps = `task?page=${page}`;
            console.log(urlKeeps);
            axios({
                method:'get',
                url:urlKeeps,
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .then(response => {
                this.keeps = response.data.tasks.data;
                this.pagination = response.data.pagination;
            })
            .catch(error => console.log(error));
        },
        deleteKeep: function(id){
            const url = `task/${id}`;
            let vm = this;
            axios.delete(url)
            .then(response => {
                console.log(response);
                vm.getKeeps();
                toastr.success('Eliminado Correctamente');
            })
        },
        createKeep: function(){
            let url = 'task', vm = this;
            axios.post(url, {
                keep:this.newKeep
            })
            .then(response => {
                vm.getKeeps();
                vm.newKeep='';
                vm.errors = [];
                $("#create").modal('hide');
                toastr.success('saved succesfully');
            })
            .catch(error=>{
                console.log(error);
                vm.errors = error.response.data;
            })
        },
        changePage: function(page){
            this.pagination.current_page = page;
            this.getKeeps(page);
        }
    }
});

